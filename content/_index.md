

#### Favorite posts and series
[C++](/tags/cppxx)&nbsp;[11](/post/cpp-11)&nbsp;[14](/post/cpp-14)&nbsp;[17](/post/cpp-17)&nbsp;[20](/post/cpp-20) &bullet;
[macOS&nbsp;Setup](/post/setup-a-new-mac) [(AS)](/post/setup-apple-silicon) &bullet;
[Azure&nbsp;DevOps](/categories/azure-devops)
([Python&nbsp;Wheels](/post/azure-devops-python-wheels)) &bullet;
[Conda-Forge&nbsp;ROOT](/post/root-conda) &bullet;
[CLI11](/tags/cli11) &bullet;
[GooFit](/tags/goofit) &bullet;
[cibuildwheel](/tags/cibuildwheel) &bullet;
[Hist](/tags/hist) &bullet;
[Python&nbsp;Bindings](/tags/bindings) &bullet;
[Python&nbsp;2&rarr;3](/post/python-3-upgrade),&nbsp;[3.8](/post/python-38) &bullet;
[SSH](/post/setting-up-ssh-forwarding/)

#### My books and workshops
[Modern&nbsp;CMake](https://cliutils.gitlab.io/modern-cmake/) &bullet;
[CMake&nbsp;Workshop](https://hsf-training.github.io/hsf-training-cmake-webpage/) &bullet;
[Computational Physics Class](https://henryiii.github.io/compclass) &bullet;
Python [CPU](https://github.com/henryiii/python-performance-minicourse),
[GPU](https://github.com/henryiii/pygpu-minicourse),
[Compiled](https://github.com/henryiii/python-compiled-minicourse) minicourses &bullet;
[Level&nbsp;Up Your Python](https://henryiii.github.io/level-up-your-python)

#### My projects
[pybind11](https://pybind11.readthedocs.io)
   ([python_example](https://github.com/pybind/python_example),
    [cmake_example](https://github.com/pybind/cmake_example),
    [scikit_build_example](https://github.com/pybind/scikit_build_example)) &bullet;
[cibuildwheel](https://cibuildwheel.readthedocs.io) &bullet;
[build](https://pypa-build.readthedocs.io) &bullet;
[scikit-build](https://github.com/scikit-build/scikit-build)
  ([cmake](https://github.com/scikit-build/cmake-python-distributions),
   [ninja](https://github.com/scikit-build/ninja-python-distributions)) &bullet;
[boost-histogram](https://github.com/scikit-hep/boost-histogram) &bullet;
[Hist](https://github.com/scikit-hep/hist) &bullet;
[UHI](https://github.com/scikit-hep/uhi) &bullet;
[Scikit-HEP/cookie](https://github.com/scikit-hep/cookie) &bullet;
[Vector](https://github.com/scikit-hep/vector) &bullet;
[CLI11](https://github.com/CLIUtils/CLI11) &bullet;
[Plumbum](https://plumbum.readthedocs.io/en/latest) &bullet;
[GooFit](https://github.com/GooFit/GooFit) &bullet;
[Particle](https://github.com/scikit-hep/particle) &bullet;
[DecayLanguage](https://github.com/scikit-hep/decaylanguage) &bullet;
[Conda-Forge&nbsp;ROOT](https://github.com/conda-forge/root-feedstock) &bullet;
[POVM](https://github.com/Princeton-Penn-Vents/princeton-penn-flowmeter) &bullet;
[Jekyll-Indico](https://github.com/iris-hep/jekyll-indico) &bullet;
[pytest&nbsp;GHA&nbsp;annotate-failures](https://github.com/utgwkk/pytest-github-actions-annotate-failures)

#### My sites
[IRIS-HEP](https://iris-hep.org) &bullet;
[Scikit-HEP](https://scikit-hep.org) ([Developer pages](https://scikit-hep.org/developer)) &bullet;
[CLARIPHY](https://clariphy.org)

{{< red >}}

#### Coronavirus links
[worldometer](https://www.worldometers.info/coronavirus/) &bullet;
[science-responds](https://science-responds.org)

{{< /red >}}
