---
title: "About the author"
date: 2017-10-10T14:17:15-05:00
---

Henry Schreiner is a Computational Physicist / Research Software Engineer in High
Energy Physics at Princeton University. He specializes in the interface between
high-performance compiled codes and interactive computation in Python, in
software distribution, and in interface design. He has previously worked on
computational cosmic-ray tomography for archaeology and high performance GPU
model fitting. He is currently a member of the IRIS-HEP project, developing
tools for the next era of the Large Hadron Collider (LHC).

He is a maintainer/core developer for pypa/build, scikit-build, cibuildwheel,
pybind11, and plubmum for Python. He is an admin of Scikit-HEP, and a lead
designer on boost-histogram, hist, UHI, vector, Particle, and DecayLanguage
packages there. He is also the lead author of the Scikit-HEP developer pages
and scikit-hep/cookie. He is the primary author of CLI11, a C++ library used by
Microsoft terminal and many others. He is also the lead web developer for
IRIS-HEP. He is also the author of Modern CMake and a variety of CMake, GPU,
and Python training courses and classes.

Pages:

* [Princeton RSE Group](https://researchcomputing.princeton.edu/software-engineering/group-members)
* [Princeton Research Computing page](https://researchcomputing.princeton.edu/people/henry-f-schreiner)
* [GHuser page](https://ghuser.io/henryiii)


-------

This site was originally at [iscinumpy.blogsplot.com](https://iscinumpy.blogspot.com). The posts have, in many cases, been re-rendered from the original Jupyter notebooks, which are now available in [the source code on GitLab](https://gitlab.com/iscinumpy/iscinumpy.gitlab.io).
