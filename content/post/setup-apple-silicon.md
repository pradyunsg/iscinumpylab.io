---
title: "Setup an Apple Silicon Mac"
date: 2021-02-11T17:00:00-04:00
categories:
  - Apple
tags:
  - macos
---

I recently got an M1 mac, and I'll be cataloging my experience with using it
for scientific software development. I'll be returning to update this page
periodically, and will eventually have a focused recommendation for Apple
Silicon setup, similar to [my Intel setup](/post/setup-a-new-mac).

<!--more-->

## Base system observations

Before getting into setup, here are my observations on the base system setup

* `python` is installed, 2.7.16, with the standard warning not to use it.
* `python3` (from the CLT) is 3.8.2 with pip 19.2.3. (macOS 11, but no change to 12).
* Note that Python 3.9 + Pip 21.0.1 are required for a proper experience on
  macOS, these must be custom Apple builds.
* `git` is 2.24.3, not bad at all. 2.30.1 for macOS 12.
* `ruby` is 2.6.3p62 and a Universal build, gives a warning about going away in
  the future. Good, built-ins get in the way of brew.



## Basic setup

Always update macOS first thing. I forgot this, so had to reinstall the CLT
_and_ wipe and reinstall brew, since it was complaining that "git" was invalid.
Hopefully that won't happen again next update...

First things first, to install pretty much anything you need the "Xcode
command-line tools" (CLT). That is triggered automatically lots of different
ways, including typing `python3` into a terminal. The best way to trigger it
is:

```bash
xcode-select --install
```

This includes all the useful open-source tools Unix developers expect, like
git, clang, [and
more](http://osxdaily.com/2014/02/12/install-command-line-tools-mac-os-x/).
You'll have to agree to the license.


## Brew installs

Next, install [HomeBrew](https://brew.sh), this is the my favorite package
manager for Macs.

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

echo 'eval $(/opt/homebrew/bin/brew shellenv)' >> /Users/henryfs/.zprofile
eval $(/opt/homebrew/bin/brew shellenv)
```

This goes into `/opt/homebrew`, as is normal for the M1, instead of the normal
`/usr/local`.

I'm stripping down my usual recommendations, non-casks and many casks work just
fine, but starting as clean as possible for now.

{{< code file="/Brewfile-arm64" language="bash" >}}

#### GH tool

You'll want to setup the `gh` tool with:

```bash
gh config set git_protocol ssh
```

## Other

### ITerm2

You should open up ITerm2 (installed above) and make it the default shell and install the terminal utilities (both in the main menu). Pin to dock. Set SauceCodePro Nerd Font as the main font (you can/should do this in the normal terminal, too).

### SSH Key

If you want to download the brew bundle (or anything, really) from GitHub, it's
a good idea to [setup an SSH Key](/post/setting-up-ssh-forwarding/). In short:

```bash
ssh-keygen
# enter a passphrase
pbcopy < ~/.ssh/id_rsa.pub
```


This will keep you from entering the passphrase as long as you are logged in:


```bash
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
```

And add the following to `.ssh/config`:

```text
Host *
  AddKeysToAgent yes
  UseKeychain yes
  IdentityFile ~/.ssh/id_rsa
```

While you are at it, feel free to set `git config --global user.name` and `git
config --global user.email`.

### Fish

If you want to use fish, add `/opt/homebrew/bin/fish` to `/etc/shells` and then run:

```bash
chsh -s /opt/homebrew/bin/fish
```

Load a new terminal in a separate tab (before closing the current one) to make sure it works.

For some reason (probably due to fish expecting `/usr/local/bin` but not expecting `/opt/homebrew/bin`), the
brew path is missing, so you'll need to add it to Fish:

```fish
fish_add_path /opt/homebrew/bin
```

Install [fisher](https://github.com/jorgebucaran/fisher):

```fish
curl https://git.io/fisher --create-dirs -sLo ~/.config/fish/functions/fisher.fish
```

This works best when you use the extended fonts in your terminal (Select SauceCodePro Nerd Font), and then run:

```bash
set -g theme_nerd_fonts yes
fisher install oh-my-fish/theme-bobthefish
```

> #### Note:
>
> Now that zsh comes default, there is less of a need to switch. You may prefer [Oh MyZsh](https://ohmyz.sh) instead. Use `sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"` to install. I still find bob-the-fish too good to pass up.

Here are a few useful environment variables to set. I don't like getting too far from the default, but these are pretty useful. These persist across logins:

```fish
set -Ux CMAKE_GENERATOR Ninja
set -Ux VIRTUAL_ENV_DISABLE_PROMPT 1
set -Ux CTEST_OUTPUT_ON_FAILURE 1
```

Use `set -UxL` to list.


## Quality of life

I like to change capslock into escape, which is in keyboard settings, and must
be done per-keyboard. Also, the keyboard repeat was very slow for some reason.

### VIM

It is important to have installed macvim from brew directly, and not the cask, or otherwise the vi command will not be changed to the new vim. Here is my vimrc, which includes directions. I used to use the ultimate VIM package, but this is much more flexible; still adjusting to fill in gaps.
Things will break if you use the system vim; you should at least run:

```bash
git config --global core.editor $(which vim)
```

to make sure you do not open the old vim instead when committing with git. Remove the `$` if you took my suggestion and are using fish.

{{< code file="/vimrc" language="vim" >}}
