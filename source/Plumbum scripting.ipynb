{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Scripting in Bash is a pain. Bash can do almost anything, and is unbeatable for small scripts, but it struggles when scaling up to doing anything close to a real world scripting problem. Python is a natural choice, especially for the scientist who already is using it for analysis. But, it's much harder to do basic tasks in Python. So you are left with scripts starting out as Bash scripts, and then becoming a mess, then being (usually poorly) ported to Python, or even worse, being run by a Python script. I've seen countless Python scripts that run Bash scripts that run real programs. I've even written one or two. It's not pretty.\n",
    "\n",
    "I recently came (back) across a really powerful library for doing efficient command line scripts in Python. It contains a set of tools that makes the four (five with color) main tasks of command line scripts simple and powerful. I will also go over the one main drawback of the library (and the possible enhancement!)."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "<!--more-->"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** The colors module is new to Plumbum in 1.6.0."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Local commands\n",
    "\n",
    "The first and foremost part of the library is a replacement for popen, subprocess, etc. of Python. I'll compare the \"correct, current\" Python standard library method and Plumbum's method."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic commands\n",
    "\n",
    "Our first task will simply be to get our feet wet with a simple command. Let's run `ls` to see the contents of the current directory. This is easy with `subprocess.call`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import subprocess"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "subprocess.call([\"echo\", \"I am a string\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What just happened? The result, zero, was the return code of the call. The output of the call went to stdout, so if we were in a terminal, we would have seen it output (and in IPython notebook, it will show up in the terminal that started the notebook). This might be what we want, but probably we wanted the value of the output. That would be `subprocess.check_output`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "b'I am a string\\n'"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "subprocess.check_output([\"echo\", \"I am a string\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can already see, this not only requires different calls for different situations, but it even gave a bytes string (which is technically correct, but almost never what you want for a shell script). The reason for the different calls is because they are shortcuts to the actual subprocess Popen object. So we really need:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "b'I am a string\\n'"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "p = subprocess.Popen(\n",
    "    [\"echo\", \"I am a string\"],\n",
    "    shell=False,\n",
    "    bufsize=512,\n",
    "    stdin=subprocess.PIPE,\n",
    "    stdout=subprocess.PIPE,\n",
    "    stderr=subprocess.PIPE,\n",
    ")\n",
    "\n",
    "outs, errs = p.communicate()\n",
    "outs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can guess, this is only a small smattering of the options you can pass (not all were needed for this call), but it gives you an idea of what is needed to work with subprocess."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at Plumbum. First, let's see the fastest method to get a command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'I am a string\\n'"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from plumbum import local, FG, BG, TF, RETCODE\n",
    "\n",
    "echo = local[\"echo\"]\n",
    "echo(\"I am a string\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, we have a local object, which represents the computer. It acts like a dictionary; if you put in a key, you get the command that would be called if you run that command in a terminal. Let's look at the object we get:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "LocalCommand(<LocalPath /bin/echo>)"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "echo"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now this is a working python object and can be called like any Python function! In fact, it can access most all of the details and power of the Popen object we saw earlier. If you don't like to repeat yourself, there is a magic shortcut for getting commands:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from plumbum.cmd import echo"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is no `echo` command in a `cmd.py` file somewhere; this dynamically does exactly what we did, calling `['echo']` on the local object. This is quicker and simpler, but it is good to know what happens behind the scenes!\n",
    "\n",
    "Plumbum also allows you to add arguments to a command without running the command; as you will soon see, this allows you to build complex commands just like bash. If you use square brackets instead of parenthesis, the command doesn't run yet (Haskal users: this is currying; Pythonistas will know it as `partial`)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "BoundCommand(LocalCommand(<LocalPath /bin/echo>), ['I am a string'])"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "echo[\"I am a string\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you are ready, you can call it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'I am a string\\n'"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "echo[\"I am a string\"]()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or, you can run it in the forground, so that the output is sent to the current terminal **as it runs** (this is the `subprocess.call` equivilent from the beginning, although non-zero return values are not handled in the same way):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from plumbum import FG\n",
    "\n",
    "echo[\"I am a string\"] & FG"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Complex commands (piping)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Stdin\n",
    "\n",
    "Now, how about input a python text string to a command? As an example, let's use the unix `dc` command. It is a desktop calculator, with reverse polish notation syntax."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from plumbum.cmd import dc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can call it using the `-e` flag followed by the calculation we want to preform, like `1 + 2`. We already know how to do that,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'3\\n'"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "dc(\"-e\", \"1 2 + p\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But, it also can be run without this flag. If we do that, we can then type (or pipe) text in from the bash shell. "
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "<div class=\"splitbox\"><div class=\"left\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In `subprocess`, we don't have a shortcut, so we have to use `Popen`, manually setting the `stdin` and `stdout` to a `subprocess` `PIPE`, and then communicate in bytes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "b'3\\n'"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "proc = subprocess.Popen([\"dc\"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)\n",
    "outs, errs = proc.communicate(\"1 2 + p\".encode(\"ascii\"))\n",
    "outs"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "</div><div class=\"right\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "Compare that to Plumbum:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'3\\n'"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(dc << \"1 2 + p\")()"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "</div><div style=\"clear:both\"></div></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Piping\n",
    "\n",
    "Of course, in bash we can pipe text from one command to another. Let's compare that (not going to even try the subprocess call here)."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "<div class=\"splitbox\"><div class=\"left\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " Since I'm using IPython, prepending a line with ! will cause it to run in bash. So, in Bash:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3\r\n"
     ]
    }
   ],
   "source": [
    "!echo \"1 2 + p\" | dc"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "</div><div class=\"right\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Plumbum:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'3\\n'"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(echo[\"1 2 + p\"] | dc)()"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "</div><div style=\"clear:both\"></div></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "If we wanted to see what that command would look like in bash, then we can call print on the unevaluated object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/bin/echo '1 2 + p' | /usr/bin/dc\n"
     ]
    }
   ],
   "source": [
    "print(echo[\"1 2 + p\"] | dc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Background execution\n",
    "\n",
    "One of the great things about Bash is the ease of \"simple\" multithreading; you can start a command in the background using the `&` character. To test this, we need a long running command that returns a value. In bash, we can make this using the following function:\n",
    "```bash\n",
    "$ fun () { sleep 3; echo 'finished'; }\n",
    "$ fun\n",
    "finished\n",
    "$ fun &\n",
    "[1] 6210\n",
    "$finished\n",
    "[1]+  Done                    fun\n",
    "```\n",
    "Here, when we ran it in the foreground, it held up our terminal until it finished. The second time we ran it, it gave us back our terminal, but we were interrupt ed three seconds later with text from the process. If we wanted to interact with the process, or wait for it to finish, etc, we could do `$!` to get the pid of the last spawned process, and then use `wait` to wait on the pid. (see [git-all.bash](https://gist.github.com/henryiii/5841984) for an example).\n",
    "\n",
    "This simplicity is not usually something that is easy to emulate in a programing language. Let's see it in plumbum. Here, I'm piping sleep (which doesn't print anything) to echo, just so I can get a slow running command, and I'm using IPython's time magic to measure the time taken:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "hi\n",
      "\n",
      "CPU times: user 5.75 ms, sys: 9.25 ms, total: 15 ms\n",
      "Wall time: 3.01 s\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "sleep = local[\"sleep\"]\n",
    "sleep_and_print = sleep[\"3\"] | echo[\"hi\"]\n",
    "print(sleep_and_print())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 3.94 ms, sys: 7.45 ms, total: 11.4 ms\n",
      "Wall time: 20.4 ms\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "bg = sleep_and_print & BG"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, bg is a `Future` object that is attached to the background process. We can call `.poll()` on it to see if it's done or `.wait()` to wait until it returns. Then, we can access the stdout and stderr of the command. (`stdout`, etc. will automatically `wait()` for you, so you can use them directly.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "hi\n",
      "\n",
      "CPU times: user 2.14 ms, sys: 1.76 ms, total: 3.9 ms\n",
      "Wall time: 2.73 s\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "print(bg.stdout)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Remote commands\n",
    "\n",
    "Besides local commands, Plumbum provides a remote class for working with remote machines via SSH in a platform independent manner. It works much like the local object, and will use the best system, including Paramiko, to do the processes. I haven't moved my scripts from pure Paramiko to Plumbum yet, but only having to learn one procedure for both local and remote machines is a huge plus (and Paramiko is fairly ugly to program in, like subprocess)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Command Line Applications\n",
    "\n",
    "Command line applications on Python already have one of the best toolkits available, argparse (C++'s Boost Program Options library is a close second). However, after seeing the highly pythonic Plumbum `cli` module, it feels repetitive and antiquated.\n",
    "\n",
    "Let's look at a command line application that takes a couple of options.\n",
    "In argparse, we would need to do the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting color_argparse.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile color_argparse.py\n",
    "import argparse\n",
    "\n",
    "def main():\n",
    "    parser = argparse.ArgumentParser(description='Echo a command in color.')\n",
    "    parser.add_argument('-c','--color', type=str,\n",
    "                       help='Color to print')\n",
    "    parser.add_argument('echo',\n",
    "                       help='The item to print in color')\n",
    "\n",
    "    args = parser.parse_args()\n",
    "    print('I should print', args.echo, 'in', args.color, \"- but I'm lazy.\")\n",
    "    \n",
    "if __name__ == '__main__':\n",
    "    main()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "I should print item in red - but I'm lazy.\n"
     ]
    }
   ],
   "source": [
    "%run color_argparse.py -c red item"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can tell from the [documentation](https://docs.python.org/3/library/argparse.html), the programs quickly grow as you try to do more advanced commands, grouping, or subcommands. Now compare to Plumbum:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting color_plumbum.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile color_plumbum.py\n",
    "from plumbum import cli\n",
    "\n",
    "class ColorApp(cli.Application):\n",
    "    color = cli.SwitchAttr(['-c','--color'], help='Color to print')\n",
    "    \n",
    "    def main(self, echo):\n",
    "        print('I should print', echo, 'in', self.color, \"- but I'm lazy.\")\n",
    "\n",
    "if __name__ == '__main__':\n",
    "    ColorApp.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "I should print item in red - but I'm lazy.\n"
     ]
    }
   ],
   "source": [
    "%run color_plumbum.py -c red item"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, we see a more natural mapping of class -> program, and also we have a lot more control over the items this way, as well. For example, if we want to add a validator, say to check existing files or to ensure a number in a range or a word in a set, we can do that on each switch. Switches can also be full fledged functions that run when the switch is set. And, we can easily extend this process to subcommands (see [git-all.py](https://gist.github.com/e194abcae39eff6ef123.git)) and it remains readable and avoids duplication."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Path manipulations\n",
    "\n",
    "Path manipulations using `os.path` functions are messy and can become involved quickly. Things that should be simple require several functions chained to get anywhere. The situation was bad enough to warrant adding an additional module to Python 3.4+, the provisional [pathlib](https://docs.python.org/3/library/pathlib.html) module. Now this is not a bad module, but you have to install a separate library on Python 2.7 or 3.3 to get it, and it has a couple of missing features. Plumbum provides a similar construct, and it is automatically available if you are already using Plumbum, and it corrects two of the three missing features. The features I'm mentioning are:\n",
    "\n",
    "* No support for manipulation of multiple extensions, like `.tar.gz`\n",
    "    * Plumbum supports an additional argument to `.with_suffix()`, default matches pathlib\n",
    "* No support for home directories\n",
    "    * Plumbum provides the `local.env.home` path\n",
    "* No support for using `open(path)` without wrapping in a `str()` call\n",
    "    * Can't be fixed unless path subclasses str (not likely for either library, see unipath), or pathlib support added to the system open function (any Python devs reading? Please?)\n",
    "    \n",
    "I would love to see the `pathlib` module adapt the `.with_suffix()` addition that Plumbum has, and add some sort of home directory expansion or path, as well.\n",
    "\n",
    "Plumbum also has the unique little trick that `//` automatically calls glob, making path composition even simpler. I doubt we'll get this added to pathlib, but I can always hope (at least, until someone removes the provisional status)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Color support (NEW)\n",
    "\n",
    "I've been working on a new color library for Plumbum. `git-all.py` has been converted to use it.\n",
    "\n",
    "Colors are used through the Styles generated by the colors object. You can get colors and atributes like this:\n",
    "```python\n",
    "from plumbum import colors\n",
    "red = colors.fg.red # Red forground color\n",
    "other_color = colors.bg(2)  # The second background color\n",
    "bold = colors.cold\n",
    "reset = colors.reset\n",
    "```\n",
    "You can directly access `colors` as if it was the `fg` object. Standard term colors can be accessed with `()`, and the 256 extended colors can be accessed with `[]` by number, name (camel case or underscore), or html code. All objects support with statements, which restores normal font (for a single `Style`, it will reset only the necessary component if possible, like `bold` or `fg` color). You can manually take the inverse (`~`) to get the undo-ing action. Calling a `Style` without any parameters will send it to `stdout`. Using `|` will wrap a string with a style (as will `[]` notation). Styles otherwise act just like normal text, so they can be added to text, etc (they are `str` subclasses, after all)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the following demo, I'll be using the HTMLCOLOR, and a with statement to capture ouput in IPython and display it as HTML. (See my upcoming post for a more elegant IPython display technique.) Also note `redirect_stdout` is new in Python 3.4, but is easy to implement in other versions if needed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from plumbum.colorlib import htmlcolors as colors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from IPython.display import display_html\n",
    "from contextlib import contextmanager, redirect_stdout\n",
    "from io import StringIO  # Python3 name\n",
    "\n",
    "\n",
    "@contextmanager\n",
    "def show_html():\n",
    "    out = StringIO()\n",
    "    with redirect_stdout(out):\n",
    "        yield\n",
    "    display_html(out.getvalue(), raw=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, inside the capture context manager, we can use COLOR just like on a terminal (save for needing to use `</br>` to break lines if we don't take advantage of the build in htmlstyle print command, and having to be careful not to use un-reset Styles)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<font color=\"#00C000\">This is in red!</font><br/>\n",
       "<font color=\"#0000C0\"><b>This is in bold blue!</b></font><br/>\n",
       "<span style=\"background-color: #FFFF00\">This is on the background!</span><br/>\n",
       "<font color=\"#0000FF\">This is also from the extented color set</font><br/>\n",
       "This is <em>emphasized</em>! (reset was needed)<br/>This is normal\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "with show_html():\n",
    "    colors.green.print(\"This is in red!\")\n",
    "    (colors.bold & colors.blue).print(\"This is in bold blue!\")\n",
    "    colors.bg[\"LightYellow\"].print(\"This is on the background!\")\n",
    "    colors[\"LightBlue\"].print(\"This is also from the extented color set\")\n",
    "    print(\n",
    "        \"This is {colors.em}emphasized{colors.em.reset}! (reset was needed)\".format(\n",
    "            colors=colors\n",
    "        ),\n",
    "        end=\"<br/>\",\n",
    "    )\n",
    "    print(\"This is normal\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Putting it together in an example: git-all"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let's look at a real world example previously mentioned: [git-all.bash](https://gist.github.com/henryiii/5841984). This is a script I wrote some time ago for checking a large number of repositories in a common folder. Due to the clever way git subcommands work, simply naming this `git-all` and putting it in your path gives your a `git all` command. It is written in very reasonable bash, IMO, and works well.\n",
    "\n",
    "## Directory manipulation\n",
    "\n",
    "Let's look at this piece by piece and see what would be required to convert it to Python. First, this script is in one of the repo's, so we need the current directory, up one."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "<div class=\"splitbox\"><div class=\"left\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Bash that's:\n",
    "\n",
    "```bash\n",
    "unset CDPATH\n",
    "SOURCE=\"${BASH_SOURCE[0]}\"\n",
    "while [ -h \"$SOURCE\" ]; do \n",
    "  DIR=\"$( cd -P \"$( dirname \"$SOURCE\" )\" && pwd )\"\n",
    "  SOURCE=\"$(readlink \"$SOURCE\")\"\n",
    "  [[ $SOURCE != /* ]] && SOURCE=\"$DIR/$SOURCE\"\n",
    "done\n",
    "DIR=\"$( cd -P \"$( dirname \"$SOURCE\" )\" && pwd )\"\n",
    "REPOLOC=$DIR/..\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(Sorry for the awful highlighting by IPython, it hates the $ in strings for Bash.)"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "</div><div class=\"right\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Converted to python,\n",
    "```python\n",
    "REPOLOC = local.path(__file__) / '..'\n",
    "```"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "</div><div style=\"clear:both\"></div></div>"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "<div class=\"splitbox\"><div class=\"left\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can find the directories that are valid repos:\n",
    "```bash\n",
    "for file in $(ls); do\n",
    "     if [[ -d $REPOLOC/$file/.git ]]; then\n",
    "     ...\n",
    "     done\n",
    "```\n",
    "And code goes here."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "</div><div class=\"right\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Python, lists are easy to use:\n",
    "```python\n",
    "valid_repos = [d / '../..' for d in local.cwd // '*/.git/config']\n",
    "```"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "</div><div style=\"clear:both\"></div></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The multiple ugly loops over all repos can easily translate into a generator:\n",
    "```python\n",
    "def git_on_all(bold=False):\n",
    "    for n,repo in enumerate(valid_repos):\n",
    "        with local.cwd(repo):\n",
    "            with color_change(n):\n",
    "                yield repo.basename\n",
    "```\n",
    "\n",
    "To use it, simply loop over `git_on_all()`:\n",
    "```python\n",
    "for repo_name in git_on_all():\n",
    "    print('The current working directory is in the', repo_name, 'repo!')\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Command line arguments\n",
    "\n",
    "We don't have a nice `cli` tool in Bash, so we have to build long if statements. We can separate each command in Python, and let the help file be built for us:\n",
    "```python\n",
    "@GitAll.subcommand(\"pull\")\n",
    "class Pull(cli.Application):\n",
    "    'Pulls all repos in the folder, not threaded.'\n",
    "    def main(self):\n",
    "        for repo in git_on_all():\n",
    "            git['pull'] & FG\n",
    "```\n",
    "\n",
    "This is `git all pull`, clean and seperated from the ugly loops in Bash."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Multithreading"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "<div class=\"splitbox\"><div class=\"left\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The fetch loop, one of the strong points of the Bash script, looks like this:\n",
    "\n",
    "```bash\n",
    "if [[ $1 == qfetch ]]\n",
    "  || [[ $1 == fetch ]]\n",
    "  || [[ $1 == status ]]; then\n",
    "   \n",
    "    for file in $(ls); do\n",
    "      if [[ -d $REPOLOC/$file/.git ]]; then\n",
    "        cd $REPOLOC/$file\n",
    "        git fetch -q &\n",
    "        spawned+=($!)\n",
    "      fi\n",
    "    done\n",
    " \n",
    "    echo -n \"Waiting for all repos to report: \"\n",
    "    for pid in ${spawned[@]}; do\n",
    "      wait $pid\n",
    "    done\n",
    "    echo \"done\"\n",
    "fi\n",
    "```"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "</div><div class=\"right\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This does a normally advanced multithreading process in a few, simple lines. In Python, we have\n",
    "```python\n",
    "def fetch():\n",
    "    bg = [(git['fetch','-q'] & BG )\n",
    "              for repo in git_on_all()]\n",
    "    printf('Waiting for the repos to report: ')\n",
    "    for fut in bg:\n",
    "        fut.wait()\n",
    "    print('done')\n",
    "```\n",
    "This is just as readable, if not more so, and doesn't need the if loop to check the input, since that's now part of the `cli` interface. The actual version in the script also can report errors in the fetch, which the Bash version can not."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "</div><div style=\"clear:both\"></div></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Colors (classic tput method)\n",
    "\n",
    "We would like to toggle colors, so each repo is in a different cyclic color. My final Bash solution was elegant:"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "<div class=\"splitbox\"><div class=\"left\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Bash (will need to run `echo -n` on these):\n",
    "```bash\n",
    "txtreset=$(tput sgr0)\n",
    "txtbold=$(tput bold)\n",
    "```"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "</div><div class=\"right\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python would be able to do the same thing (will only need to run these in the foreground, with `& FG` ):\n",
    "```python\n",
    "txtreset=tput['sgr0']\n",
    "txtbold=tput['bold']\n",
    "```\n",
    "\n",
    "Though with the `plumbum.color` library, we don't have to."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "</div><div style=\"clear:both\"></div></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Color changing is easy to implement with a Python context manager:\n",
    "```python\n",
    "@contextmanager\n",
    "def color_change(color):\n",
    "    txtreset & FG\n",
    "    txtbold & FG\n",
    "    tput['setaf',color%6+1] & FG\n",
    "    try:\n",
    "        yield\n",
    "    finally:\n",
    "        txtrst & FG\n",
    "```\n",
    "The try/finally block allows this to restore our color, even if it throws an exception! This is tremendously better than the Bash version, which leaves the color on the terminal if you make a mistake. A nice example of context managers can be found [on Jeff Preshing's blog](http://preshing.com/20110920/the-python-with-statement-by-example/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use it to wrap parts of the code that print in a color:\n",
    "```python\n",
    "with colorchange(tput('setaf',2), bold=True):\n",
    "    print('This will be in color number 2')\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Colors (new method)\n",
    "\n",
    "Plumbum has a new colors tool, and this is how you would use in in this script.\n",
    "\n",
    "```python\n",
    "from plumbum import colors\n",
    "```\n",
    "\n",
    "Colors can be generated cyclically by number, and combinations of color and attributes can be put in a with statement, too:\n",
    "\n",
    "```python\n",
    "with(colors.fg[1:7][n%6] & colors.bold):\n",
    "```\n",
    "\n",
    "And, we can simply unbold:\n",
    "```python\n",
    "colors.bold.reset.print(git('diff-files', '--name-status', '-r', '--ignore-submodules', '--'))\n",
    "```\n",
    "And that's it! All the benefits we had from before are here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Final Comparison\n",
    "\n",
    "I'll be using functions in the Python version to make it clear what each git call does, and making the Python version cleaner in a few ways that I could also apply to the Bash script. So this is not meant to be a 1:1 comparison. In my defense, Bash users tend to avoid functions or other clean programming practices. \n",
    "\n",
    "Most of the extra lines are from the Python functions. Also, I've improved a couple of commands for git for current best practices. I've also avoided using FG for the print commands, so that I can control the color and the long-output paging (If you change `print()` for `& FG`, the output would match the Bash script). Here is the script: [git-all.py](https://gist.github.com/henryiii/e194abcae39eff6ef123).\n",
    "\n",
    "> **Note:** You might want to look at the history of that script, as I'll probaby update it occasionally as I start using it.\n",
    "\n",
    "Notice that it is very clear what each part of the `cli` part of the script, and it's easy to add a feature or extend it. The long for loops are nicely abstracted into iterators.\n",
    "\n",
    "Also, there may be bugs for a few days while I start using this instead of my bash script. Also, it must be renamed to `git-all` with no extension for `git all status` etc. to work."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bonus: Possible improvement: argcomplete support"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One last thing: The one drawback to Plumbum over argparse is due to one enhancement package for argparse. I think a great addition to the Plumbum library would be [argcomlete](https://github.com/kislyuk/argcomplete) support. If you've never seen argcomplete, it's a bash completion extension for argparse. Argcomplete allows you to add validators, and will suggest completions when pressing tab on the command line.\n",
    "\n",
    "Adding support to Plumbum would not be that hard, and probably wouldn't even require argcomplete to be installed. The Argcomplete API requires three things:\n",
    "\n",
    "* `#ARGCOMPLETE_OK` near the top of a script\n",
    "* Special output piped to several channels (8 and 9, I believe) of the terminal when the `_ARGCOMPLETE` special environment variable is set, and then exits before calling `.main()`.\n",
    "* The ability to predict the next completion\n",
    "\n",
    "The first one is easy, and wouldn't require anything from Plumbum. The second would be a simple addition to a new method `cli.Application.argcomplete(self)` that could be overridden to remove or customize argcomplete support. The final one is the hard one, the prediction of the possible completions. If that can be done, support could be added.\n",
    "\n",
    "Because support would be added into Plumbum itself, you wouldn't have to use the monkey patching that argcomplete has to use to inject itself into argparse. You would still use the same bash hooks that argcomplete uses, so it would work along side it, being called in the same way."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.4.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
